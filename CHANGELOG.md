# [1.12.0](https://gitlab.com/natural-solutions/reneco/portal/compare/v1.11.1...v1.12.0) (2023-11-14)


### Features

* Instance fictive are not displayed ([51927ee](https://gitlab.com/natural-solutions/reneco/portal/commit/51927ee55c4a081da850be40b04f75d30b574b27))

## [1.11.1](https://gitlab.com/natural-solutions/reneco/portal/compare/v1.11.0...v1.11.1) (2023-11-14)


### Bug Fixes

* add readonly theme ([18bbe42](https://gitlab.com/natural-solutions/reneco/portal/commit/18bbe42c42fe055de4d61118eb5122c7faf481f8))

# [1.11.0](https://gitlab.com/natural-solutions/reneco/portal/compare/v1.10.0...v1.11.0) (2023-10-18)


### Features

* changed readonly system in securite ([55fc8d1](https://gitlab.com/natural-solutions/reneco/portal/commit/55fc8d17c6df089d47a5ea7b25554a061ec79a7b))

# [1.10.0](https://gitlab.com/natural-solutions/reneco/portal/compare/v1.9.0...v1.10.0) (2023-10-17)


### Features

* Modification of the documentation/{id} route in the portal to specify the content-type in the header according to the extension stored for the file in question ([0580b7b](https://gitlab.com/natural-solutions/reneco/portal/commit/0580b7bb04f2020cdd9794a3eb061864ea7cf520))

# [1.9.0](https://gitlab.com/natural-solutions/reneco/portal/compare/v1.8.1...v1.9.0) (2023-09-19)


### Features

* init Health check service ([005f7a7](https://gitlab.com/natural-solutions/reneco/portal/commit/005f7a7630a5ce364e62f0cbe66d89142ead1890))

## [1.8.1](https://gitlab.com/natural-solutions/reneco/portal/compare/v1.8.0...v1.8.1) (2023-08-23)


### Bug Fixes

* update import of reneco-font ([e5ff7d0](https://gitlab.com/natural-solutions/reneco/portal/commit/e5ff7d0511472576706f0456f2738a9d182066e1))

# [1.8.0](https://gitlab.com/natural-solutions/reneco/portal/compare/v1.7.1...v1.8.0) (2023-07-25)


### Features

* poc getCookieContent ([9ad5e8c](https://gitlab.com/natural-solutions/reneco/portal/commit/9ad5e8cbfc4509661056eff6236b38525db662e5))
* temporarily does not expects a secret ([9fcc535](https://gitlab.com/natural-solutions/reneco/portal/commit/9fcc535a1ab3bd8f5cfeba8b74d04feca53c0f7d))

## [1.7.1](https://gitlab.com/natural-solutions/reneco/portal/compare/v1.7.0...v1.7.1) (2023-06-29)


### Bug Fixes

* modified deprecated token root reference for deletion on logout ([9727c99](https://gitlab.com/natural-solutions/reneco/portal/commit/9727c99be04c56c750452c7c6fd1da4d30933d7f))

# [1.7.0](https://gitlab.com/natural-solutions/reneco/portal/compare/v1.6.4...v1.7.0) (2023-06-13)


### Bug Fixes

* Boolean to BIT ([7a50fe3](https://gitlab.com/natural-solutions/reneco/portal/commit/7a50fe32c61682f44150a805fb217287b4e52198))
* merge conflict ([6cc3edf](https://gitlab.com/natural-solutions/reneco/portal/commit/6cc3edff11cabecf4d5079b442fb37534d6a250b))
* quickly fixed the case of a non existing env asked ([bc6048b](https://gitlab.com/natural-solutions/reneco/portal/commit/bc6048b4b0431cfa5e8b74ddaa7c694dcb685603))


### Features

* merged dev with release ([02eabea](https://gitlab.com/natural-solutions/reneco/portal/commit/02eabeacf624ef9b6d81ec1cf30c3ac5025a1d1f))
* merged release with master ([b922d08](https://gitlab.com/natural-solutions/reneco/portal/commit/b922d08e3e7e23596926a7d0af577629daa0e79f))

## [1.6.4](https://gitlab.com/natural-solutions/reneco/portal/compare/v1.6.3...v1.6.4) (2023-06-13)


### Bug Fixes

* remove instance of inactive application ([245162e](https://gitlab.com/natural-solutions/reneco/portal/commit/245162e47ae5733d0ce534e139bdd9c53cd11710))

## [1.6.3](https://gitlab.com/natural-solutions/reneco/portal/compare/v1.6.2...v1.6.3) (2023-03-20)


### Bug Fixes

* fix indentation in build_info_for_mobile_app and add environment in reqParams ([33e525c](https://gitlab.com/natural-solutions/reneco/portal/commit/33e525c53f0fd06e311c890e2ce30d951b5ad2ac))

## [1.6.2](https://gitlab.com/natural-solutions/reneco/portal/compare/v1.6.1...v1.6.2) (2023-02-09)


### Bug Fixes

* left join rather than normal join ... ([9ade60d](https://gitlab.com/natural-solutions/reneco/portal/commit/9ade60d4fd0b2481b343e349719d3fb0ca3f9de4))
* removed useless print ([3c4cb61](https://gitlab.com/natural-solutions/reneco/portal/commit/3c4cb617b4ba94f6863eebfc0b022d870533a66f))
* removed useless prints ([6569a2d](https://gitlab.com/natural-solutions/reneco/portal/commit/6569a2dc328a8eae4e4b4550c4e5b45b8b345726))

## [1.6.1](https://gitlab.com/natural-solutions/reneco/portal/compare/v1.6.0...v1.6.1) (2023-01-10)


### Bug Fixes

* add distinct to avoid duplicate instance on portal ([4c8eefc](https://gitlab.com/natural-solutions/reneco/portal/commit/4c8eefca0a9a1ef1a25c2a215e3a6bfa9fefdebf))

# [1.6.0](https://gitlab.com/natural-solutions/reneco/portal/compare/v1.5.0...v1.6.0) (2022-11-02)


### Bug Fixes

* send always result as dict for route instance?path ([dc03d29](https://gitlab.com/natural-solutions/reneco/portal/commit/dc03d297421f558cc4de4f76cd09567a05f22c63))
* update only the ID of DET instance by MGT id ([39748ae](https://gitlab.com/natural-solutions/reneco/portal/commit/39748aec1b40a1c17073e18df5199ec34db2dd31))


### Features

* added the tab_name also to the route instance/:id ([0e93db6](https://gitlab.com/natural-solutions/reneco/portal/commit/0e93db66102bef597dc46e34b8a7a492c65dc1fd))

# [1.5.0](https://gitlab.com/natural-solutions/reneco/portal/compare/v1.4.0...v1.5.0) (2022-10-19)


### Features

* update reneco-font with npm ([ba79034](https://gitlab.com/natural-solutions/reneco/portal/commit/ba790342d9690dd91a271686251f9868761f9d33))

# [1.4.0](https://gitlab.com/natural-solutions/reneco/portal/compare/v1.3.2...v1.4.0) (2022-09-14)


### Features

* the route instance?path={} should always return the instance of management if the path corresponds to a repro application ([d14f4d9](https://gitlab.com/natural-solutions/reneco/portal/commit/d14f4d985124462339c5d8c1836acb49b229d82d))

## [1.3.2](https://gitlab.com/natural-solutions/reneco/portal/compare/v1.3.1...v1.3.2) (2022-03-22)


### Bug Fixes

* update reneco-font to 2.1.6 ([61a6b01](https://gitlab.com/natural-solutions/reneco/portal/commit/61a6b014847e344ee216e0d5330c7a3844540ac1))

## [1.3.1](https://gitlab.com/natural-solutions/reneco/portal/compare/v1.3.0...v1.3.1) (2022-03-15)


### Bug Fixes

* link between device and instance for mobile ([6514e81](https://gitlab.com/natural-solutions/reneco/portal/commit/6514e81ed5ada7ba22358882f5de02b46fa3192e))

# [1.3.0](https://gitlab.com/natural-solutions/reneco/portal/compare/v1.2.0...v1.3.0) (2022-01-26)


### Features

* add querystring to filter instance by path ([5312572](https://gitlab.com/natural-solutions/reneco/portal/commit/531257200354acf1c5627bcc8beebdb8d8a98ef3))
* remove instance id from access and refresh token ([aef041e](https://gitlab.com/natural-solutions/reneco/portal/commit/aef041e65c13abde2750fb17b2e0d8c62862a6a7))

# [1.2.0](https://gitlab.com/natural-solutions/reneco/portal/compare/v1.1.0...v1.2.0) (2022-01-26)


### Bug Fixes

* add class column + use ([3f26605](https://gitlab.com/natural-solutions/reneco/portal/commit/3f266058713fc12828b280a895ee35eb603989bc))
* send class name ([e971474](https://gitlab.com/natural-solutions/reneco/portal/commit/e971474ec231465b1d0a384eb6caf535c47d7564))


### Features

* add theme name to instance route ([033159b](https://gitlab.com/natural-solutions/reneco/portal/commit/033159b2c39683aa22af9d80aaf081a1a3d31e25))

# [1.1.0](https://gitlab.com/natural-solutions/reneco/portal/compare/v1.0.0...v1.1.0) (2021-12-15)


### Features

* add route for getting instance/{id} ([4626551](https://gitlab.com/natural-solutions/reneco/portal/commit/4626551d5f62edc994e899467f136f9ff0af2dd4))

# 1.0.0 (2021-11-29)


### Bug Fixes

* add certifi to install in env ([7881592](https://gitlab.com/natural-solutions/reneco/portal/commit/7881592862762a551f3e65fff201fdf3b72482d0))
* copy bower_components to the final image ([2125cf3](https://gitlab.com/natural-solutions/reneco/portal/commit/2125cf32b3bee65b3afc8e49b883b7709f47c174))
* network fixed inside docker container ([6e68bec](https://gitlab.com/natural-solutions/reneco/portal/commit/6e68bec41f637d6e1cf388740ffe9605c860c12d))
