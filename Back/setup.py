from setuptools import setup, find_packages

requires = [
    'marshmallow==3.3.0',
    'marshmallow-sqlalchemy==0.21.0',
    'pyjwt[crypto]==2.2.0',
    'pymssql==2.1.4',
    'pyramid==1.10.4',
    'sqlalchemy==1.3.12',
    'transaction==3.0.0',
    'waitress==1.4.2',
    'webargs==6.0.0b3',
    'psutil==5.9.5'
    ]

setup(
    name='ns_portal',
    version='0.5',
    description='ns_portal',
    classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
    ],
    author='',
    author_email='',
    url='',
    keywords='web wsgi bfg pylons pyramid',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    test_suite='ns_portal',
    install_requires=requires,
    entry_points="""\
    [paste.app_factory]
    main = ns_portal:main
    [console_scripts]
    initialize_ns_portal_db = ns_portal.scripts.initializedb:main
    """
    )
