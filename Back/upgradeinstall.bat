call env\Scripts\activate
call python -m pip install --upgrade pip setuptools

rem Installation en mode production (non editable) si la variable d'environnement NS_PYTHON_ENV existe et a pour valeur "prod"
IF "%NS_PYTHON_ENV%" == "prod" (
    call pip install .
) ELSE (
    call pip install -e . 
)
