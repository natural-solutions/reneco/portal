from sqlalchemy import (
    engine_from_config,
    event
)
from sqlalchemy.orm import (
    sessionmaker,
    configure_mappers
)
from sqlalchemy.exc import (
    DisconnectionError
) 
from ns_portal.database.meta import (
    Main_Db_Base,
    Log_Db_Base
) # noqa
__all__ = [
    "Main_Db_Base",
    "Log_Db_Base"
]

# import or define all models here to ensure they are attached to the
# Base.metadata prior to any initialization routines
from .main_db import * # noqa
from .log_db import * # noqa

# run configure_mappers after defining all of the models to ensure
# all relationships can be setup
configure_mappers()

MAINDBKEY = 'maindb'
LOGDBKEY = 'logdb'

saved_config = None

def includeme(config):
    global saved_config
    saved_config= config
    load_engines()


def __create_engine(params, key):
    engine = engine_from_config(
        params,
        prefix='sqlalchemy' + '.' + key + '.'
        )

    @event.listens_for(engine, "engine_connect")
    def ping_connection(connection, branch):
        if branch:
            return
        if not connection.closed:
            try:
                connection.scalar("SELECT 1")
            except:
                load_engines()

    return engine


def __createEngines(myConfig):
    engines = {
        Main_Db_Base: __create_engine(
            params=myConfig,
            key=MAINDBKEY
            ),
        Log_Db_Base: __create_engine(
            params=myConfig,
            key=LOGDBKEY
        )
    }
    return engines


def __create_session(myConfig, engines):
    return sessionmaker(
        autoflush=True,
        binds=engines
    )


def get_session(request):
    session = request.registry.get('dbsession_factory')()
    session.begin_nested()

    def cleanup(request):
        if request.exception is not None:
            session.rollback()
            session.close()
        else:
            try:
                session.commit()
            except Exception as e:
                print(e)
                session.rollback()
                request.response.status_code = 500
            finally:
                session.close()

    request.add_finished_callback(cleanup)

    return session


def new_request(event):
    request = event.request
    request.set_property(get_session, 'dbsession', reify=True)

def load_engines(config = None):
    if (config is None):
        config = saved_config
    myConfig = config.get_settings()

    engines = __createEngines(
        myConfig=myConfig
    )

    session_factory = __create_session(
        myConfig=myConfig,
        engines=engines
        )

    config.registry['dbsession_factory'] = session_factory
    # for each new request we gonna add
    # dbsession attribute
    config.add_subscriber(
        'ns_portal.database.new_request',
        'pyramid.events.NewRequest'
    )
