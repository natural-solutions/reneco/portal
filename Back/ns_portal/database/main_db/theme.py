from ns_portal.database.meta import (
    Main_Db_Base
)
from sqlalchemy import (
    Column,
    Integer,
    ForeignKey,
    String
)
from sqlalchemy.dialects.mssql import (
    BIT
)


class Theme(Main_Db_Base):
    __tablename__ = 'Theme'

    ID = Column(
        Integer,
        primary_key=True
    )
    Name = Column(
        String(100),
        nullable=False
    )
    Application_ID = Column(
        Integer,
        ForeignKey("Application.ID"),
        nullable=False
    )
    Primary_Color = Column(
        String(100),
        nullable=False
    )
    Is_Principal = Column(
        BIT,
        nullable=False,
        server_default='1'
    )
    Is_Active = Column(
        BIT,
        nullable=False,
        server_default='1'
    )
    Class = Column(
        String(100),
        nullable=False
    )
