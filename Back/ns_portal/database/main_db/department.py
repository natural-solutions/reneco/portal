from ns_portal.database.meta import (
    Main_Db_Base
)
from sqlalchemy import (
    Column,
    DateTime,
    Integer,
    ForeignKey,
    text
)
from sqlalchemy.dialects.mssql import (
    BIGINT
)


class Department(Main_Db_Base):
    __tablename__ = 'Department'

    ID = Column(
        Integer,
        primary_key=True
    )
    Name = Column(
        BIGINT,
        ForeignKey("Language_Link.ID"),
        nullable=False
    )
    System_Date = Column(
        DateTime,
        nullable=True,
        server_default=text("GETDATE()")
    )
