from ns_portal.database.meta import (
    Main_Db_Base
)
from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey
)

class Device_Instance_Dev(Main_Db_Base):
    __tablename__ = 'Device_Instance_Dev'

    Device_ID = Column(
        Integer,
        ForeignKey("Device.ID"),
        nullable=False,
        primary_key=True
    )
    Instance_ID = Column(
        Integer,
        ForeignKey("Instance.ID"),
        nullable=False,
        primary_key=True
    )
    Environment = Column(
        String(5),
        nullable=False,
        server_default='DEV'
    )
