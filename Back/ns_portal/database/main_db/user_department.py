from ns_portal.database.meta import (
    Main_Db_Base
)
from sqlalchemy import (
    Column,
    DateTime,
    Integer,
    ForeignKey
)


class User_Department(Main_Db_Base):
    __tablename__ = 'User_Department'

    ID = Column(
        Integer,
        primary_key=True
    )
    User_ID = Column(
        Integer,
        ForeignKey("Users.ID"),
        nullable=False
    )
    Department_ID = Column(
        Integer,
        ForeignKey("Department.ID"),
        nullable=False
    )
    Start_Date = Column(
        DateTime,
        nullable=False
    )
