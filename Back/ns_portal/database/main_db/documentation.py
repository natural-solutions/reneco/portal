from ns_portal.database.meta import (
    Main_Db_Base
)
from sqlalchemy import (
    Column,
    DateTime,
    Integer,
    String,
    text
)
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.dialects.mssql import (
    VARBINARY
)

class Documentation(Main_Db_Base):
    __tablename__ = 'Documentation'

    ID = Column(
        Integer,
        primary_key=True
    )
    Name = Column(
        String(250),
        nullable=False
    )
    Content = Column(
        VARBINARY,
        nullable=False
    )
    Application_ID = Column(
        Integer,
        ForeignKey("Application.ID"),
        nullable=False
    )
    Role_ID = Column(
        Integer,
        ForeignKey("Role.ID"),
        nullable=True
    )
    System_Date = Column(
        DateTime,
        nullable=True,
        server_default=text("GETDATE()")
    )
    Extention = Column(
        String(5),
        nullable=False
    )
