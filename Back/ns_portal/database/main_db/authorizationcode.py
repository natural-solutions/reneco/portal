from ns_portal.database.meta import (
    Main_Db_Base
)
from sqlalchemy import (
    Column,
    DateTime,
    Integer,
    text
)


class AuthorizationCode(Main_Db_Base):
    __tablename__ = 'AuthorizationCode'

    ID = Column(
        Integer,
        primary_key=True
    )
    creationDate = Column(
        DateTime,
        nullable=False,
        server_default=text("GETDATE()")
    )
