from ns_portal.database.meta import (
    Main_Db_Base
)
from sqlalchemy import (
    Column,
    DateTime,
    Integer,
    String,
    text
)
from sqlalchemy.dialects.mssql import (
    BIGINT,
    BIT
)
from sqlalchemy.sql.schema import ForeignKey


class Server(Main_Db_Base):
    __tablename__ = 'Server'

    ID = Column(
        BIGINT,
        primary_key=True
    )
    Name = Column(
        String(150),
        nullable=False
    )
    Applicative_Domain_Name = Column(
        String(150),
        nullable=False
    )
    SQL_Instance_Name = Column(
        String(150),
        nullable=False
    )
    Site_ID = Column(
        Integer,
        ForeignKey("Site.ID"),
        nullable=False
    )
    System_Date = Column(
        DateTime,
        nullable=False,
        server_default=text('GETDATE()')
    )
    Is_Master = Column(
        BIT,
        nullable=False,
        server_default='0'
    )
    Server_Group = Column(
        String(50),
        nullable=True
    )
    Is_Active = Column(
        BIT,
        nullable=False,
        server_default='1'
    )
