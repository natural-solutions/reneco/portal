from ns_portal.database.meta import (
    Main_Db_Base
)
from sqlalchemy import (
    Column,
    Integer,
    String
)
from sqlalchemy.dialects.mssql import (
    BIGINT
)


class Language_Value(Main_Db_Base):
    __tablename__ = 'Language_Value'

    ID = Column(
        Integer,
        primary_key=True
    )
    Value = Column(
        String(500),
        nullable=False
    )
    Language_ID = Column(
        BIGINT,
        nullable=False
    )
    Language_Link_ID = Column(
        BIGINT,
        nullable=False
    )
