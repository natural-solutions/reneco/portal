from ns_portal.database.meta import (
    Main_Db_Base
)
from sqlalchemy import (
    Column,
    Integer
)
from sqlalchemy.dialects.mssql import (
    BIT
)
from sqlalchemy.sql.sqltypes import String


class Device(Main_Db_Base):
    __tablename__ = 'Device'

    ID = Column(
        Integer,
        primary_key=True
    )
    Serial_Number = Column(
        String(250),
        nullable=False,
        unique=True
    )
    Is_Read_Only = Column(
        BIT,
        nullable=False,
        server_default='1'
    )
    Is_Active = Column(
        BIT,
        nullable=False,
        server_default='1'
    )
    Environment = Column(
        String(5),
        nullable=False,
        server_default='PROD'
    )
