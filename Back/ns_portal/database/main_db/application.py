from ns_portal.database.meta import (
    Main_Db_Base
)
from sqlalchemy import (
    Column,
    DateTime,
    Integer,
    ForeignKey,
    String
)
from sqlalchemy.dialects.mssql import (
    BIT,
    BIGINT
)


class Application(Main_Db_Base):
    __tablename__ = 'Application'

    ID = Column(
        Integer,
        primary_key=True
    )
    Name = Column(
        String(50),
        nullable=True
    )
    Description = Column(
        BIGINT,
        ForeignKey('Language_Link.ID'),
        nullable=False
    )
    Client_ID = Column(
        String(500),
        nullable=False
    )
    TApp_Documentation = Column(
        String(250),
        nullable=True,
        server_default=''
    )
    Favicon = Column(
        String(250),
        nullable=True
    )
    System_Date = Column(
        DateTime,
        nullable=True
    )
    Is_Active = Column(
        BIT,
        nullable=False,
        server_default='1'
    )
