from ns_portal.database.meta import (
    Main_Db_Base
)
from sqlalchemy import (
    Column,
    String
)
from sqlalchemy.dialects.mssql import (
    BIGINT
)


class Language(Main_Db_Base):
    __tablename__ = 'Language'

    ID = Column(
        BIGINT,
        primary_key=True
    )
    Name = Column(
        String(100),
        nullable=False
    )
    Abbreviation = Column(
        String(15),
        nullable=False
    )
