from ns_portal.database.meta import (
    Main_Db_Base
)
from sqlalchemy import (
    Column,
    DateTime,
    Integer,
    String,
    text
)
from sqlalchemy.dialects.mssql import (
    BIT,
    VARBINARY
)


class Site(Main_Db_Base):
    __tablename__ = 'Site'

    ID = Column(
        Integer,
        primary_key=True
    )
    Name = Column(
        String(150),
        nullable=False
    )
    Locality = Column(
        String(50),
        nullable=False
    )
    Country = Column(
        String(50),
        nullable=False
    )
    Back_Portal_Image = Column(
        VARBINARY,
        nullable=True
    )
    Logo_Portal_Image = Column(
        VARBINARY,
        nullable=True
    )
    Back_Home_Page_Image = Column(
        VARBINARY,
        nullable=True
    )
    Portal_Name = Column(
        String(20),
        nullable=False,
        server_default=''
    )
    Portal_URL = Column(
        String(250),
        nullable=True
    )
    Centre_Name = Column(
        String(250),
        nullable=True
    )
    System_Date = Column(
        DateTime,
        nullable=False,
        server_default=text('GETDATE()')
    )
    Is_Active = Column(
        BIT,
        nullable=False,
        server_default='1'
    )
    Centre = Column(
        String(10),
        nullable=True
    )
