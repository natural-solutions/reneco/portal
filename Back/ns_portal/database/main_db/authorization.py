from ns_portal.database.meta import (
    Main_Db_Base
)
from sqlalchemy import (
    Column,
    Integer
)
from sqlalchemy.dialects.mssql import (
    BIT
)
from sqlalchemy.sql.schema import ForeignKey


class Authorization(Main_Db_Base):
    __tablename__ = 'Authorization'

    ID = Column(
        Integer,
        primary_key=True
    )
    Instance_ID = Column(
        Integer,
        ForeignKey("Instance.ID"),
        nullable=False
    )
    User_ID = Column(
        Integer,
        ForeignKey("Users.ID"),
        nullable=False
    )
    Role_ID = Column(
        Integer,
        ForeignKey("Role.ID"),
        nullable=False
    )
    Is_Observer = Column(
        BIT,
        nullable=False
    )
