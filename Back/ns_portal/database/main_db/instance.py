from ns_portal.database.meta import (
    Main_Db_Base
)
from sqlalchemy import (
    Column,
    DateTime,
    Integer,
    String,
    text
)
from sqlalchemy.dialects.mssql import (
    BIT
)

from sqlalchemy.sql.schema import ForeignKey


class Instance(Main_Db_Base):
    __tablename__ = 'Instance'

    ID = Column(
        Integer,
        primary_key=True
    )
    Name = Column(
        String(250),
        nullable=False
    )
    Path = Column(
        String(250),
        nullable=True
    )
    Theme = Column(
        String(500),
        nullable=True
    )
    Database = Column(
        String(500),
        nullable=True
    )
    Display_Order = Column(
        Integer,
        nullable=True
    )
    Application_ID = Column(
        Integer,
        ForeignKey("Application.ID"),
        nullable=False
    )
    Application_Server_ID = Column(
        Integer,
        ForeignKey("Server.ID"),
        nullable=True
    )
    Tab_Name = Column(
        String(250),
        nullable=True,
        server_default=''
    )
    System_Date = Column(
        DateTime,
        nullable=False,
        server_default=text("GETDATE()")
    )
    Is_Active = Column(
        BIT,
        nullable=False,
        server_default='1'
    )
    Theme_ID = Column(
        Integer,
        ForeignKey("Theme.ID"),
        nullable=False
    )
    Database_Server_ID = Column(
        Integer,
        ForeignKey("Server.ID"),
        nullable=False
    )
    Mobile_Path = Column(
        String(250),
        nullable=True
    )
    Is_Fictive = Column(
        BIT,
        nullable=False,
        server_default='0'
    )