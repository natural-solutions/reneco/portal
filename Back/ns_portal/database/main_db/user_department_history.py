from ns_portal.database.meta import (
    Main_Db_Base
)
from sqlalchemy import (
    Column,
    DateTime,
    Integer,
    ForeignKey
)


class User_Department_History(Main_Db_Base):
    __tablename__ = 'User_Department_History'

    ID = Column(
        Integer,
        primary_key=True
    )
    Start_Date = Column(
        DateTime,
        nullable=False
    )
    User_ID = Column(
        Integer,
        ForeignKey("Users.ID"),
        nullable=False
    )
    Department_ID = Column(
        Integer,
        ForeignKey("Department.ID"),
        nullable=False
    )
    System_Date = Column(
        DateTime,
        nullable=True
    )
    End_Date = Column(
        DateTime,
        nullable=True
    )
