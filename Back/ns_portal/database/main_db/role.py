from ns_portal.database.meta import (
    Main_Db_Base
)
from sqlalchemy import (
    Column,
    DateTime,
    String,
    text
)
from sqlalchemy.dialects.mssql import (
    BIGINT
)
from sqlalchemy.sql.schema import ForeignKey


class Role(Main_Db_Base):
    __tablename__ = 'Role'

    ID = Column(
        BIGINT,
        primary_key=True
    )
    Name = Column(
        String(100),
        nullable=False
    )
    Description = Column(
        BIGINT,
        ForeignKey('Language_Link.ID'),
        nullable=False
    )
    System_Date = Column(
        DateTime,
        nullable=False,
        server_default=text("GETDATE()")
    )
