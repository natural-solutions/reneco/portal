from ns_portal.database.meta import (
    Main_Db_Base
)
from sqlalchemy import (
    Column,
    DateTime,
    Integer,
    ForeignKey,
    String
)
from sqlalchemy.dialects.mssql import (
    BIT,
    BIGINT
)


class Users(Main_Db_Base):
    __tablename__ = 'Users'

    ID = Column(
        Integer,
        primary_key=True
    )
    Last_Name = Column(
        String(100),
        nullable=True
    )
    First_Name = Column(
        String(100),
        nullable=True
    )
    System_Date = Column(
        DateTime,
        nullable=True
    )
    Login = Column(
        String(255),
        nullable=False
    )
    Password = Column(
        String(50),
        nullable=True
    )
    Is_Access_Granted = Column(
        BIT,
        nullable=False
    )
    Language = Column(
        BIGINT,
        ForeignKey('Language.ID'),
        nullable=False,
        server_default='1'
    )
    User_Initials = Column(
        String(100),
        nullable=True
    )
