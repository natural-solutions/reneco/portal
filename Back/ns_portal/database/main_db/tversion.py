from ns_portal.database.meta import (
    Main_Db_Base
)
from sqlalchemy import (
    Column,
    DateTime,
    String
)


class TVersion(Main_Db_Base):
    __tablename__ = 'TVersion'

    TVer_FileName = Column(
        String(100),
        nullable=False,
        primary_key=True  # Workaround
    )
    # sqlalchemy need a primary key for all table
    # https://stackoverflow.com/questions/34283259/how-to-define-a-table-without-primary-key-with-sqlalchemy
    TVer_Date = Column(
        DateTime,
        nullable=False
    )
    TVer_DbName = Column(
        String(50),
        nullable=True
    )
