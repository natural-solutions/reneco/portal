from ns_portal.database.meta import (
    Main_Db_Base
)
from sqlalchemy import (
    Column,
    Integer
)


class Language_Link(Main_Db_Base):
    __tablename__ = 'Language_Link'

    ID = Column(
        Integer,
        primary_key=True
    )
