from zope.interface import (
    implementer
)
from pyramid.interfaces import (
    IAuthenticationPolicy
)
from pyramid.authentication import (
    CallbackAuthenticationPolicy
)
from pyramid.authorization import (
    ACLAuthorizationPolicy
)
from pyramid.security import (
    Everyone,
    Authenticated
)
from ns_portal.utils import (
    myDecode
)
from ns_portal.database.main_db import (
    Language,
    Users
)
from sqlalchemy.orm.exc import (
    MultipleResultsFound
)


@implementer(IAuthenticationPolicy)
class MyAuthenticationPolicy(CallbackAuthenticationPolicy):

    """
    A custom authentification policy
    This object will be create once

    Constructor Arguments

    ``header``
        A dict or a JSON string with the JWT Header data.

    """

    def __init__(
        self,
        cookieTokenPrivateKey=None,
        cookieTokenPublicKey=None,
        cookieTokenTime=None,
        accessTokenPrivateKey=None,
        accessTokenPublicKey=None,
        accessTokenTime=None,
        codeTokenPrivateKey=None,
        codeTokenPublicKey=None,
        codeTokenTime=None,
        refreshTokenPrivateKey=None,
        refreshTokenPublicKey=None,
        refreshTokenTime=None,
        cookie_name=None,
        TSit_Name=None
    ):

        self.cookie_name = cookie_name
        self.TSit_Name = TSit_Name

        if cookieTokenPrivateKey is None:
            raise ValueError('cookieTokenPrivateKey should not be empty')
        else:
            self.cookieTokenPrivateKey = bytes(
                cookieTokenPrivateKey,
                encoding='utf-8'
                )
        if cookieTokenPublicKey is None:
            raise ValueError('cookieTokenPublicKey should not be empty')
        else:
            self.cookieTokenPublicKey = bytes(
                cookieTokenPublicKey,
                encoding='utf-8'
                )
        if cookieTokenTime is None:
            raise ValueError('cookieTokenTime should not be empty')
        else:
            self.cookieTokenTime = int(cookieTokenTime)

        if accessTokenPrivateKey is None:
            raise ValueError('accessTokenPrivateKey should not be empty')
        else:
            self.accessTokenPrivateKey = bytes(
                accessTokenPrivateKey,
                encoding='utf-8'
                )
        if accessTokenPublicKey is None:
            raise ValueError('accessTokenPublicKey should not be empty')
        else:
            self.accessTokenPublicKey = bytes(
                accessTokenPublicKey,
                encoding='utf-8'
                )
        if accessTokenTime is None:
            raise ValueError('accessTokenTime should not be empty')
        else:
            self.accessTokenTime = int(accessTokenTime)

        if codeTokenPrivateKey is None:
            raise ValueError('codeTokenPrivateKey should not be empty')
        else:
            self.codeTokenPrivateKey = bytes(
                codeTokenPrivateKey,
                encoding='utf-8'
                )
        if codeTokenPublicKey is None:
            raise ValueError('codeTokenPublicKey should not be empty')
        else:
            self.codeTokenPublicKey = bytes(
                codeTokenPublicKey,
                encoding='utf-8'
                )
        if codeTokenTime is None:
            raise ValueError('codeTokenTime should not be empty')
        else:
            self.codeTokenTime = int(codeTokenTime)

        if refreshTokenPrivateKey is None:
            raise ValueError('refreshTokenPrivateKey should not be empty')
        else:
            self.refreshTokenPrivateKey = bytes(
                refreshTokenPrivateKey,
                encoding='utf-8'
                )
        if refreshTokenPublicKey is None:
            raise ValueError('refreshTokenPublicKey should not be empty')
        else:
            self.refreshTokenPublicKey = bytes(
                refreshTokenPublicKey,
                encoding='utf-8'
                )
        if refreshTokenTime is None:
            raise ValueError('refreshTokenTime should not be empty')
        else:
            self.refreshTokenTime = int(refreshTokenTime)

        self.callback = self.getClaims

    def getClaims(self, allClaims, request):
        return allClaims

    def effective_principals(self, request):
        principals = [Everyone]
        user = self.authenticated_userid(request)
        if user:
            principals += [Authenticated]

        return principals

    def checkUserInDb(self, request, claims):
        idUser = claims.get('sub')
        query = request.dbsession.query(
            Users.First_Name,
            Users.ID,
            Users.Last_Name,
            Users.Login,
            Users.User_Initials,
            Language.Name.label('Language')
            )
        query = query.join(Language)

        query = query.filter(
                    Users.ID == idUser
                )
        try:
            res = query.one_or_none()
            if res:
                res = res._asdict()
        except MultipleResultsFound:
            raise MultipleResultsFound()

        return res

    def authenticated_userid(self, request):
        '''
        this function will check if datas (sub and role )
        in cookie's payload in request
        match datas in database when you do the request

        for later or specific case
        '''

        userCookieClaims = self.unauthenticated_userid(request)
        if userCookieClaims is not None:
            user = self.checkUserInDb(request, userCookieClaims)
            return user

        return None

    def unauthenticated_userid(self, request):
        userCookieClaims = None
        cookie = request.cookies.get(self.cookie_name)
        if cookie:
            userCookieClaims = self.extractClaimsFromCookie(cookie)

        return userCookieClaims

    def extractClaimsFromCookie(self, jwt):
        claims = None

        claims = myDecode(
            token=jwt,
            secret=self.cookieTokenPublicKey
            )
        return claims

    def remember(self, response, token):

        '''
        call by login view
        given userid we gonna generate claims add in payload
        will generate cookie headers for response

        '''
        Sec = 1
        Mins = 60 * Sec
        Hours = 60 * Mins
        Days = 24 * Hours
        # Weeks = 7 * Days

        maxAge = 1 * Days

        # secure=False
        # becareful if activated cookie
        # will travel only on securized canal (HTTPS)
        # httponly=True
        # security manipulate cookie by javascript in client
        response.set_cookie(
            name=self.cookie_name,
            value=token,
            max_age=maxAge,
            path='/',
            domain=None,
            secure=False,
            httponly=False,
            comment=None,
            expires=None,
            overwrite=False,
            samesite=None
        )

    def forget(self, request):
        '''
        Delete a cookie from the client.  Note that ``path`` and ``domain``
        must match path and domain the cookie was originally set.

        Will sets the cookie to the empty string, and ``max_age=0`` so
        that it should expire immediately.
        '''
        request.response.delete_cookie(
            name=self.cookie_name,
            path='/',
            domain=None
        )


class myAuthorizationPolicy(ACLAuthorizationPolicy):
    def authenticated_userid(self, request):
        print("authenticated_userid policy")
        return []

    def effective_principals(self, request):
        print("effective_principals policy")
        return []
