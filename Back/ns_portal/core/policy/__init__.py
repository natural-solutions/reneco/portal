from .authentificationPolicy import (
        MyAuthenticationPolicy,
        myAuthorizationPolicy
)


def includeme(config):
    authorizationPolicy = myAuthorizationPolicy()
    config.set_authorization_policy(authorizationPolicy)

    customSettings = meaningConfig(config)

    authentificationPolicy = MyAuthenticationPolicy(**customSettings)
    config.set_authentication_policy(authentificationPolicy)

    # from now all view added to project will have 'read' permission by default
    # but BE CAREFUL and read the doc
    # https://docs.pylonsproject.org/projects/pyramid/en/latest/narr/security.html#setting-a-default-permission
    # short way :
    # use NO_PERMISSION_REQUIRED for overwrite this default permission
    config.set_default_permission('read')


def meaningConfig(config):
    '''
    pick params with need in *.ini file
    '''
    settings = config.get_settings()

    if settings.get('RENECO.SECURITE.TSIT_NAME') is None:
        '''
        '''
        raise Exception(
            'You mus\'t have this key RENECO.SECURITE.TSIT_NAME'
            'defined in your *.ini file'
        )

    return {
            "cookieTokenPrivateKey": settings.get(
                'JWTSECURITY.COOKIETOKENPRIVATEKEY',
                None
                ),
            "cookieTokenPublicKey": settings.get(
                'JWTSECURITY.COOKIETOKENPUBLICKEY',
                None
                ),
            "cookieTokenTime": settings.get(
                'JWTSECURITY.COOKIETOKENTIME',
                None
                ),
            "accessTokenPrivateKey": settings.get(
                'JWTSECURITY.ACCESSTOKENPRIVATEKEY',
                None
                ),
            "accessTokenPublicKey": settings.get(
                'JWTSECURITY.ACCESSTOKENPUBLICKEY',
                None
                ),
            "accessTokenTime": settings.get(
                'JWTSECURITY.ACCESSTOKENTIME',
                None
                ),
            "codeTokenPrivateKey": settings.get(
                'JWTSECURITY.CODETOKENPRIVATEKEY',
                None
                ),
            "codeTokenPublicKey": settings.get(
                'JWTSECURITY.CODETOKENPUBLICKEY',
                None
                ),
            "codeTokenTime": settings.get(
                'JWTSECURITY.CODETOKENTIME',
                None
                ),
            "refreshTokenPrivateKey": settings.get(
                'JWTSECURITY.REFRESHTOKENPRIVATEKEY',
                None
                ),
            "refreshTokenPublicKey": settings.get(
                'JWTSECURITY.REFRESHTOKENPUBLICKEY',
                None
                ),
            "refreshTokenTime": settings.get(
                'JWTSECURITY.REFRESHTOKENTIME',
                None
                ),
            "cookie_name": settings.get(
                'JWTSECURITY.COOKIENAME',
                None
                ),
            "TSit_Name": settings.get(
                'RENECO.SECURITE.TSIT_NAME'
                )
    }
