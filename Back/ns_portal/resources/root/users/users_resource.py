from ns_portal.core.resources import (
    MetaEndPointResource
)
from ns_portal.database.main_db import (
    Users
)
from pyramid.security import (
    Allow,
    Everyone
)


class UsersResource(MetaEndPointResource):

    __acl__ = [
        (Allow, Everyone, 'read')
        ]

    def GET(self):
        query = self.request.dbsession.query(
            Users.Login.label('fullname')
            )
        query = query.filter(Users.Is_Access_Granted == 1)
        query = query.order_by(Users.Login)
        res = query.all()

        self.request.response.json_body = [row._asdict() for row in res]
        return self.request.response
