from ns_portal.core.resources import (
    MetaRootResource
)
from ns_portal.database.main_db import (
    Site
)
from marshmallow import (
    Schema,
    EXCLUDE,
    fields
)
from pyramid.security import (
    Allow,
    Everyone
)


class siteSchema(Schema):
    img = fields.Integer(
        default=1,
        missing=1
        )

    class Meta:
        unknown = EXCLUDE


class SiteResource(MetaRootResource):

    __acl__ = [
        (Allow, Everyone, 'read')
        ]

    def GET(self):
        qsParams = self.__parser__(
            args=siteSchema(),
            location='querystring'
        )

        dictConfigIni = getattr(self.request.registry, 'settings')

        query = self.request.dbsession.query(
            Site.Name.label('title'),
            Site.Country.label('country'),
            Site.Locality.label('locality'),
            Site.Centre_Name.label('legend'),
            Site.Portal_Name.label('label')
        )
        if qsParams.get('img') == 1:
            query = query.add_columns(
                    Site.Back_Portal_Image.label('imgBackPortal'),
                    Site.Logo_Portal_Image.label('imgLogoPortal'),
                    Site.Back_Home_Page_Image.label('imgBackHomePage')
            )

        query = query.filter(
            Site.Name == dictConfigIni.get(
                'RENECO.SECURITE.TSIT_NAME',
                'Web')
                )
        res = query.one_or_none()

        return res._asdict()
