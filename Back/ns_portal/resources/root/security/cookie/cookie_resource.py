from ns_portal.core.resources import (
    MetaEmptyResource
)

from marshmallow import (
    Schema,
    fields,
    EXCLUDE
)

from pyramid.security import (
    Allow,
    Everyone
)

from pyramid.httpexceptions import (
    HTTPBadRequest
)

from ns_portal.utils import (
    getPayload
)

import configparser

class CookieSchema(Schema):
    userid = fields.String(required=True)

    class Meta:
        unknown = EXCLUDE

class CookieResource(MetaEmptyResource):

    __acl__ = [
        (Allow, Everyone, 'read'),
        (Allow, Everyone, 'CORS')
    ]

    def GET(self):
        # TODO > Re-set the secret system later
        # secretHeader = 'Admin-secret'
        # config = configparser.ConfigParser()
        # config.read('development.ini')
        # secretValue = config.get('secrets', 'Admin-secret')
            
        # try:
        #     secretHeaderValue = self.request.headers.get(secretHeader, None)
        #     if (secretHeaderValue is None or secretHeaderValue != secretValue):
        #         raise Exception("Bad admin secret")
        #     else:
        #         userid = self.request.params.get("userid", None)
        #         token = getPayload(
        #             idUser=userid,
        #             request=self.request
        #         )

        #     return token
        # except Exception as e:
        #     return HTTPBadRequest("Generic error:" + str(e))
        try:
            userid = self.request.params.get("userid", None)
            token = getPayload(
                idUser=userid,
                request=self.request
            )
            return token
        except Exception as e:
            return HTTPBadRequest("Generic error:" + str(e))
        
