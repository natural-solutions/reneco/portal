from ns_portal.core.resources import (
    MetaEndPointResource
)
from marshmallow import (
    Schema,
    fields,
    EXCLUDE,
    ValidationError
)
from ns_portal.database.main_db import (
    Users,
    Device
)
from pyramid.httpexceptions import HTTPForbidden
from sqlalchemy.orm.exc import (
    MultipleResultsFound
)
from pyramid.security import (
    Allow,
    Everyone,
    remember
)
from ns_portal.database.main_db.instance import Instance
from ns_portal.database.main_db.device_instance_prod import Device_Instance_Prod
from ns_portal.database.main_db.device_instance_dev import Device_Instance_Dev
from ns_portal.utils import (
    getCookieToken
)
from pyramid.response import (
    Response
)


class loginSchema(Schema):
    username = fields.String(required=True)
    password = fields.String(required=True)
    UUID_Mobile = fields.String(required=False)
    environment= fields.String(required=False)

    class Meta:
        unknown = EXCLUDE


class LoginResource(MetaEndPointResource):

    __acl__ = [
        (Allow, Everyone, 'create'),
        (Allow, Everyone, 'CORS')
    ]

    def validateUserCredentials(self, data):
        columns_user = [
            Users.ID,
            Users.Login,
            Users.Is_Access_Granted
        ]
        query = self.request.dbsession.query(Users)
        query = query.with_entities(*columns_user)
        query = query.filter(
            (Users.Login == data.get('username')),
            (Users.Password == data.get('password'))
        )
        try:
            res = query.one_or_none()
        except MultipleResultsFound:
            raise ValidationError({
                "error": (
                    'your username and password are'
                    ' not unique in db'
                    ' please report it to an admin'
                    )
                })
        if res:
            # this key is added after validation
            return res
        else:
            raise ValidationError({
                "error": (
                    'your username and/or password'
                    ' are wrongs'
                    )
                })

    def build_info_for_mobile_app(self, UUID_found, env_request):
        to_ret = []
        query = self.request.dbsession.query(Device)
        query = query.filter(
            Device.Serial_Number == UUID_found,
            Device.Is_Active == True
        )
        try:
            res = query.one_or_none()

            if (res):
                if (env_request == 'PROD'):
                    Device_Instance = self.request.dbsession.query(Device_Instance_Prod).filter(Device_Instance_Prod.Device_ID == res.ID).all()
                elif (env_request == 'DEV'):
                    Device_Instance = self.request.dbsession.query(Device_Instance_Dev).filter(Device_Instance_Dev.Device_ID == res.ID).all()
                else:
                    raise ValidationError({
                        "error_msg": (
                            'Wrong environment requested...'
                            ' Please report the problem to an admininistrator'
                            ),
                        "error_code": ('wrong_environment')
                        })
                instances = self.request.dbsession.query(Instance).filter(Instance.ID.in_([o.Instance_ID for o in Device_Instance])).all()

        except MultipleResultsFound:
            raise ValidationError({
                "error_msg": (
                    'Impossible, multiple instances found for this device in database'
                    ' Please report the problem to an admininistrator'
                    ),
                "error_code": ('impossible_multiple_instance')
                })

        if res:

            if instances:
                for instance in instances:
                    mobile_path = getattr(instance, 'Mobile_Path', None)
                    instance_name = getattr(instance, 'Name', None)

                    if mobile_path and instance_name:
                        to_ret.append({'Application_Path': mobile_path, 'Instance_Name': instance_name})
                    else:
                        raise ValidationError({
                            "error_msg": (
                                'Mobile_Path and/or Name not defined for linked instance'
                                ' Please report the problem to an admininistrator'
                                ),
                            "error_code": ('no_path_or_name_for_instance')
                            })

            else:
                raise ValidationError({
                    "error_msg": (
                        'The device must be linked to an instance'
                        ' Please report the problem to an admininistrator'
                        ),
                    "error_code": ('no_instance_found')
                    })

        else:
            raise ValidationError({
                "error_msg": (
                    'This device is not registered in db'
                    ' Please report the problem to an admininistrator'
                    ),
                "error_code": ('no_device_in_db')
                })

        return to_ret

    def POST(self):
        reqParams = self.__parser__(
            args=loginSchema(),
            location='form'
        )

        userFound = self.validateUserCredentials(data=reqParams)
        json_body_response = {}

        if userFound:
            user_can_access = getattr(
                userFound,
                'Is_Access_Granted',
                False
                )
            if not user_can_access:
                return HTTPForbidden()

            token = getCookieToken(
                idUser=getattr(userFound, 'ID'),
                request=self.request
            )

            if 'UUID_Mobile' in reqParams:
                UUID_found = reqParams.get('UUID_Mobile', None)
                env_request = reqParams.get('environment', None)
                if UUID_found:
                    json_body_response = self.build_info_for_mobile_app(
                        UUID_found=UUID_found,
                        env_request=env_request
                    )
                else:
                    raise ValidationError({
                        "error_msg": (
                            'UUID_Mobile value is undefined'
                            ),
                        "error_code": ('no_UUID_Mobile')
                    })
            # remove Content-Length header
            self.request.response.headers.pop('Content-Length')
            resp = Response(
                status=200,
                json_body=json_body_response,
                headers=self.request.response.headers
                )
            remember(
                resp,
                token
                )
            self.request.response = resp
            return self.request.response
        else:
            raise ValidationError({
                "error": (
                    'your username and/or password'
                    ' are wrongs'
                    )
                })
