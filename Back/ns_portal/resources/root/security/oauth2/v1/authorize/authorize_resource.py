from ns_portal.core.resources import (
    MetaEndPointResource
)
from ns_portal.utils.utils import (
    my_get_authentication_policy
)
from marshmallow import (
    Schema,
    fields,
    EXCLUDE,
    ValidationError
)
from pyramid.security import (
    Allow,
    Authenticated
)
from ns_portal.database.main_db import (
    Instance,
    Application,
    Authorization,
    Users,
    Role,
    Site,
    Server
)
from sqlalchemy.orm.exc import (
    MultipleResultsFound
)
from ns_portal.utils import (
    getCodeToken
)


class authorizeSchema(Schema):
    client_id = fields.String(required=True)
    redirect_uri = fields.String(required=True)

    class Meta:
        unknown = EXCLUDE


class AuthorizeResource(MetaEndPointResource):

    __acl__ = [
        (Allow, Authenticated, 'create')
    ]

    def validateSchema(self, data):
        client_id = data.get('client_id')
        redirect_uri = data.get('redirect_uri')
        policy = my_get_authentication_policy(self.request)
        tsiteName = getattr(policy, 'TSit_Name')
        userId = self.request.authenticated_userid.get('ID')

        colsToRet = [
            Instance.ID.label('Instance_ID'),
            Instance.Name.label('Instance_Name'),
            Instance.Path,
            Instance.Theme,
            Instance.Database,
            Instance.Display_Order,
            Application.Client_ID,
            Application.Description,
            Role.Name.label('Role_Name'),
            Users.ID.label('User_ID'),
            Users.Login,
            Users.Language,
            Site.Name.label('Site_Name'),
            Authorization.Is_Observer
        ]

        VAllUsersApplications = self.request.dbsession.query(Instance)
        VAllUsersApplications = VAllUsersApplications.join(Application)
        VAllUsersApplications = VAllUsersApplications.join(
            Authorization,
            Instance.ID == Authorization.Instance_ID
            )
        VAllUsersApplications = VAllUsersApplications.join(Role)
        VAllUsersApplications = VAllUsersApplications.join(Users)
        VAllUsersApplications = VAllUsersApplications.join(
            Server,
            Instance.Application_Server_ID == Server.ID
            )
        VAllUsersApplications = VAllUsersApplications.join(
            Site,
            Server.Site_ID == Site.ID
        )
        VAllUsersApplications = VAllUsersApplications.with_entities(*colsToRet)

        VAllUsersApplications = VAllUsersApplications.filter(
                (Site.Name == tsiteName),
                (Users.ID == userId),
                (Role.Name != 'Interdit'),
                (Application.Client_ID == client_id),
                (Instance.Path == redirect_uri)
            )
        VAllUsersApplications = VAllUsersApplications.order_by(
            Instance.Display_Order
            )

        try:
            res = VAllUsersApplications.one_or_none()
        except MultipleResultsFound:
            raise MultipleResultsFound()
        if res:
            return res._asdict()
        else:
            raise ValidationError({
                "error": (
                    'your client_id and/or redirect_uri'
                    ' are wrongs'
                    )
                })

    def POST(self):
        reqParams = self.__parser__(
            args=authorizeSchema(),
            location='json'
        )

        result = self.validateSchema(data=reqParams)

        code = getCodeToken(
            idUser=self.request.authenticated_userid.get('ID'),
            request=self.request
        )

        self.request.response.json_body = {
            "code": code
        }
        return self.request.response
