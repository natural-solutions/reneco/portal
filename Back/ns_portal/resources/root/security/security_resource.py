from ns_portal.core.resources import (
    MetaRootResource
)
from .oauth2 import (
    OAuth2Resource
)
from .cookie import (
    CookieResource
)

class SecurityResource(MetaRootResource):

    __ROUTES__ = {
        'oauth2': OAuth2Resource,
        'cookiecontent': CookieResource
    }
