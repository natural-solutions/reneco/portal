from pyramid.security import (
    Allow, Authenticated
)
from pyramid.response import Response
from sqlalchemy.orm import Query, aliased
from ns_portal.core.resources import (
    MetaCollectionResource,
    MetaEndPointResource
)
from ns_portal.database import Documentation, Application, Instance, Authorization


documentation : Documentation = aliased(Documentation)
application : Application = aliased(Application)
class DocumentationItem(MetaEndPointResource):

    def get_mime_type(self, extention: str):
        if (extention == "txt"):
            return "text/plain"
        elif (extention == "docx"):
            return "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        elif (extention == "doc"):
            return "application/msword"
        elif (extention == "pdf"):
            return "application/pdf"

    def build_query(self):
        query = self.__parent__.build_query()
        query = query.filter(documentation.ID == self.__name__)
        return query

    def GET(self):
        res = self.build_query().one_or_none()
        if res : 
            return Response(
                body=res.Content,
                content_type= self.get_mime_type(res.Extention),
            )
        else :
            []


class DocumentationCollection(MetaCollectionResource):

    __ROUTES__ = {
        '{id}':  DocumentationItem
    }
    __acl__ = [
        (Allow, Authenticated, 'read')
        ]
    
    __specialKey__ = '{id}'

    def build_query(self):
        dbsession = self.request.dbsession

        user_id = self.request.authenticated_userid.get('ID')
        dbsession = self.request.dbsession

        query = Query(entities=[documentation], session=dbsession)
        query = query.join(application)

        subquery = Query(entities=[Instance], session=dbsession)
        subquery = subquery.join(Authorization)
        subquery = subquery.filter(Authorization.User_ID == user_id )
        subquery = subquery.filter(Instance.Application_ID == application.ID)
        subquery = subquery.filter(Authorization.Role_ID == documentation.Role_ID)
        subquery = subquery.exists()
        query = query.filter(subquery)

        return query


    def GET(self):
        client_id = self.request.params.get("client_id", None)

        query = self.build_query()
        query = query.filter(application.Client_ID == client_id)

        to_ret = []
        for row in query:
            item = {
                "id": row.ID,
                "name": row.Name,
                "label": row.Name,
            }
            to_ret.append(item)
        return to_ret