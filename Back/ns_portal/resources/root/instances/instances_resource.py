from ns_portal.core.resources import (
    MetaEndPointResource,
    MetaCollectionResource
)
from ns_portal.database.main_db import (
    Instance,
    Application,
    Authorization,
    Users,
    Role,
    Server,
    Site,
    Theme
)
from ns_portal.utils.utils import (
    my_get_authentication_policy
)
from pyramid.security import (
    Allow,
    Authenticated
)
from marshmallow import (
    ValidationError
)
from sqlalchemy import (
    case,
    func
)
from sqlalchemy.orm import (
    aliased
)
from sqlalchemy.orm.exc import (
    MultipleResultsFound
)

from pyramid.response import Response

class InstanceResource(MetaEndPointResource):

    __acl__ = [
        (Allow, Authenticated, 'read')
        ]

    def GET(self):
        theme2 = aliased(Theme)
        colsToRet = [
            case(
                [(theme2.ID == None, Theme.ID)],
                else_=theme2.ID
            ).label('ID'),
            case(
                [(theme2.ID == None, Theme.Primary_Color)],
                else_=theme2.Primary_Color
            ).label('Primary_Color'),
            case(
                [(theme2.ID == None, Theme.Class)],
                else_=theme2.Class
            ).label('Class'),
            Instance.Tab_Name,
            Instance.Name
        ]
        query = self.request.dbsession.query(Instance)
        query = query.join(Application)
        query = query.join(
            Theme,
            Application.ID == Theme.Application_ID
        )
        query = query.join(
            theme2,
            Instance.Theme_ID == theme2.ID,
            isouter=True
        )
        query = query.with_entities(*colsToRet)
        query = query.filter(
            Instance.ID == self.__name__
        )
        try:
            res = query.one_or_none()
            if res:
                objects = res._asdict()
                return {
                    'ID': self.__name__,
                    'Theme': { "ID": objects["ID"],
                        "Primary_Color": objects["Primary_Color"],
                        "Class": objects["Class"]
                    },
                    'Tab_Name': objects["Tab_Name"] if objects["Tab_Name"] else objects["Name"]
                }
            else:
                self.request.response.status_code = 404
                self.request.response.json_body = {
                    "error_msg": (
                        f'{self.__parent__.__name__}/{self.__name__} not found'
                    ),
                    "error_code": ('not_found')
                }
                self.request.response.content_type = 'application/json'
                return self.request.response
        except MultipleResultsFound:
            raise ValidationError({
                "error_msg": (
                    'Impossible multiple rows for this instance in db'
                    ' please report it to an admin'
                    ),
                "error_code": ('impossible_multiple_rows')
            })


class InstancesResource(MetaCollectionResource):

    __acl__ = [
        (Allow, Authenticated, 'read')
        ]

    __ROUTES__ = {
        '{id}':  InstanceResource
    }

    __specialKey__ = '{id}'

    def GET(self):
        path = self.request.params.get("path", None)
        policy = my_get_authentication_policy(self.request)
        tsiteName = getattr(policy, 'TSit_Name')
        userId = self.request.authenticated_userid.get('ID')
        themeDefault = aliased(Theme)
        subTheme = aliased(Theme)

        colsToRet = [
            Instance.ID,
            Instance.Name,
            Instance.Tab_Name,
            Instance.Path,
            Theme.Primary_Color.label('Theme'),
            themeDefault.Primary_Color.label('DefaultTheme'),
            Instance.Database,
            Instance.Display_Order,
            Instance.Is_Active,
            Application.Client_ID,
            Application.Description,
            Application.Favicon,
            Role.Name.label('Role_Name'),
            Users.ID.label("user_id"),
            Users.Login,
            Users.Language,
            Site.Name.label('Site_Name'),
            Authorization.Is_Observer,
            Site.Centre
        ]

        VAllUsersApplications = self.request.dbsession.query(Instance)
        VAllUsersApplications = VAllUsersApplications.join(Application)
        VAllUsersApplications = VAllUsersApplications.join(Authorization)
        VAllUsersApplications = VAllUsersApplications.join(Role)
        VAllUsersApplications = VAllUsersApplications.join(Users)
        VAllUsersApplications = VAllUsersApplications.join(
            Server,
            Instance.Application_Server_ID == Server.ID
            )
        VAllUsersApplications = VAllUsersApplications.join(Site)
        VAllUsersApplications = VAllUsersApplications.join(
            Theme,
            Theme.ID == Instance.Theme_ID,
            isouter=True
        )
        VAllUsersApplications = VAllUsersApplications.join(
            themeDefault,
            themeDefault.Application_ID == Application.ID,
            isouter=True
        )

        VAllUsersApplications = VAllUsersApplications.with_entities(*colsToRet)

        VAllUsersApplications = VAllUsersApplications.filter(
                (Application.Is_Active == True),
                (Site.Name == tsiteName),
                (Users.ID == userId),
                (Role.Name != 'Interdit'),
                (Instance.Is_Active == True),
                (Instance.Is_Fictive == False),
                (themeDefault.ID == self.request.dbsession.query(func.min(subTheme.ID)).filter(subTheme.Application_ID == Application.ID))
            )

        if path:
            result = VAllUsersApplications.filter(
                Instance.Path == path
            ).first()
            
            if result is not None:
                if result.Client_ID == "Reproduction_login":
                    temp_result = VAllUsersApplications.filter(
                        Application.Client_ID == result.Client_ID,
                        Instance.Database == result.Database,
                        Instance.Name.like("%Management%")
                    ).first()
                    result = result._asdict()
                    temp_result = temp_result._asdict()
                    result["ID"] = temp_result["ID"]
                    return [result]
                return [result._asdict()]
            else:
                return Response(
                    status=400,
                    content_type='application/json',
                    charset='utf-8',
                    body='{exception}'.format(exception="There was a problem with the path you passed as parameter!")
                )

        VAllUsersApplications = VAllUsersApplications.order_by(
            Instance.Display_Order
            )

        VAllUsersApplications = VAllUsersApplications.distinct()
        
        result = VAllUsersApplications.all()
            
        return [row._asdict() for row in result]
