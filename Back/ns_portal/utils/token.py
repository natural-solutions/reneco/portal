import jwt
import datetime
from ns_portal.database.main_db import (
    Instance,
    Application,
    Authorization,
    Language,
    Users,
    Role,
    Server,
    Site
)
from sqlalchemy.orm.exc import (
    MultipleResultsFound
)
from pyramid.httpexceptions import (
    HTTPConflict
)
from ns_portal.utils.utils import (
    my_get_authentication_policy
)


# we follow the RFC7519
# look at https://tools.ietf.org/html/rfc7519#section-4.1
# for all definitions


def myEncode(payload, secret, algorithm):
    return jwt.encode(payload, secret, algorithm=algorithm)


def myDecode(token, secret):
    payloadValided = False
    try:
        payloadValided = jwt.decode(
            token,
            secret,
            algorithms=['RS512']
        )
    except jwt.ExpiredSignatureError:
        raise jwt.ExpiredSignatureError(
            'You take too much time for getting your token.',
            'You need to login again'
        )
    except jwt.InvalidTokenError:
        raise jwt.InvalidTokenError(
            'Exception when decode()'
        )
    except jwt.DecodeError:
        raise jwt.DecodeError(
            'We canno\'t decode your token'
        )
    except jwt.InvalidSignatureError:
        raise jwt.InvalidSignatureError(
            'Your token’s signature doesn’t match'
            ' the one provided as part of the token'
        )
    return payloadValided


def getParamsFromPolicy(request, key):
    policy = my_get_authentication_policy(request)
    secret = getattr(policy, key + 'PrivateKey')
    algorithm = 'RS512'
    time = getattr(policy, key + 'Time')

    return (secret, algorithm, time)

def getPayload(idUser, request):
    secret, algorithm, time = getParamsFromPolicy(
        request=request,
        key='cookieToken'
    )

    return buildPayload(
        idUser=idUser,
        request=request,
        timeAddForExp=time
    )


def getCookieToken(idUser, request):
    secret, algorithm, time = getParamsFromPolicy(
        request=request,
        key='cookieToken'
    )

    payload = buildPayload(
        idUser=idUser,
        request=request,
        timeAddForExp=time
    )

    return myEncode(payload, secret, algorithm)


def getCodeToken(idUser, request):
    secret, algorithm, time = getParamsFromPolicy(
        request=request,
        key='codeToken'
    )

    now = datetime.datetime.now()
    nowInTimeStampSeconds = int(now.timestamp())

    payload = {
        'sub': str(idUser),
        'exp': nowInTimeStampSeconds + time
    }

    return myEncode(payload, secret, algorithm=algorithm)


def buildPayload(idUser, request, timeAddForExp):
    policy = my_get_authentication_policy(request)
    tsiteName = getattr(policy, 'TSit_Name')

    columns_user = [
        Users.ID,
        Users.Login,
        Language.Abbreviation.label('Language'),
        Users.Is_Access_Granted
    ]
    query = request.dbsession.query(Users)
    query = query.join(Language)
    query = query.with_entities(*columns_user)
    query = query.filter(
        (Users.ID == idUser)
    )
    try:
        user = query.one_or_none()
        if user is None:
            return HTTPConflict(
                headers={
                    "content_type": 'application/json',
                    "charset": 'utf-8',
                },
                body=f'ID: {idUser} doesn''t exist'
            )
    except MultipleResultsFound:
        return HTTPConflict(
            headers={
                "content_type": 'application/json',
                "charset": 'utf-8',
            },
            body=f'ID: {idUser} not unique in db'
        )

    user_id = getattr(user, 'ID')
    user_login = getattr(user, 'Login')
    user_language = getattr(user, 'Language')

    colsToRet = [
        Instance.Name.label('Instance_Name'),
        Role.Name.label('Role_Name')
    ]

    query = request.dbsession.query(Instance)
    query = query.join(Application)
    query = query.join(
        Authorization,
        Instance.ID == Authorization.Instance_ID
    )
    query = query.join(Role)
    query = query.join(
        Server,
        Instance.Application_Server_ID == Server.ID
    )
    query = query.join(
        Site,
        Server.Site_ID == Site.ID
    )
    query = query.with_entities(*colsToRet)

    query = query.filter(
            (Site.Name == tsiteName),
            (Authorization.User_ID == user_id),
            (Role.Name != 'Interdit')
    )
    query = query.order_by(
        Instance.Display_Order
    )

    result = query.all()
    roles = {}
    for row in result:
        instance_name = getattr(row, 'Instance_Name')
        role_name = getattr(row, 'Role_Name')
        roles[instance_name] = role_name

    now = datetime.datetime.now()
    nowInTimeStampSeconds = int(now.timestamp())

    payload = {}
    payload.update({
        "iss": "NSportal",
        "sub": str(user_id),
        "username": user_login,
        "userlanguage": user_language,
        'exp': nowInTimeStampSeconds + timeAddForExp,
        "roles": roles
    })

    return payload


def getAccessToken(idUser, request):
    secret, algorithm, time = getParamsFromPolicy(
        request=request,
        key='accessToken'
    )

    payload = buildPayload(
        idUser=idUser,
        request=request,
        timeAddForExp=time
    )

    return myEncode(payload, secret, algorithm=algorithm)


def getRefreshToken(idUser, request):
    secret, algorithm, time = getParamsFromPolicy(
        request=request,
        key='refreshToken'
    )

    now = datetime.datetime.now()
    nowInTimeStampSeconds = int(now.timestamp())

    payload = {
        'sub': str(idUser),
        'exp': nowInTimeStampSeconds + time
    }

    return myEncode(payload, secret, algorithm=algorithm)
