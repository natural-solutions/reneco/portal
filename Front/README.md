### Créer un nouveau thème

1. Se rendre dans l'application sécurité depuis le portail

2. Lors du choix du thème pour l'instance, si le thème n'a jamais été créé, alors l'écrire dans le champ text à côté de la sélection de thème.

3. Une fois le thème créé et associé à l’instance, se rendre dans dossier du projet NsPortal, trouver le fichier Front/app/base/home/lyt-tile.js et rajouter la case qui correspond à votre thème créé précédemment. Exemple si le thème se prénomme « CentralMonitoring » alors :
```js
case 'CentralMonitoring':
 this.model.set({'icon' : 'reneco-CentralMonitoring'});
break;
```
Ceci a pour but de définir l’icône créée au préalable dans la rénéco-font

4. Se rendre dans le fichier  Front/app/styles/ui/_tile.less et ajouter une class avec une variable, ceci représente la couleur de fond qu’aura l’icône, dans notre exemple :
```css
.CentralMonitoring{
 background : @CentralMonitoring;
}
```
5. Pour finir, créer la variable et lui associer une couleur dans le fichier Front/app/styles/_theme.less toujours dans notre exemple :
```css
@CentralMonitoring : #4B6099;
```
