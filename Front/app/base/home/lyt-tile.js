define(['marionette', 'i18n'],
function(Marionette) {
  'use strict';

  return Marionette.LayoutView.extend({
    template: 'app/base/home/tpl/tpl-tile.html',
    className: 'tile small-tile',

    initialize: function(){
      if(!this.model.get('Favicon')){
        this.model.set({'Favicon' : 'reneco-help'});
      }
      // set hostname
      var hostname  = window.location.hostname ;
      var appliPath = this.model.get('Path');
      var newPath = appliPath.replace("@@hostname@@", 'http://' + hostname);
      this.model.set('Path', newPath);
    }
  });



});
