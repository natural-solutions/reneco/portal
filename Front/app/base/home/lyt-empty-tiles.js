define(['marionette', 'i18n'],
function(Marionette) {
  'use strict';

  return Marionette.ItemView.extend({
    template: 'app/base/home/tpl/tpl-empty-tiles.html',
    className: '',
    onRender: function() {
      this.$el.i18n();
    }
  })

});